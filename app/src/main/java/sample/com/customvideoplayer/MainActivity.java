package sample.com.customvideoplayer;

import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {

    private static final String TEST_URL = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";

    UniversalVideoView mVideoView;
    UniversalMediaController mMediaController;
    private String TAG = "tag";

    FrameLayout video_layout;
    LinearLayout llbottom;



    private int cachedHeight;
    private boolean isFullscreen;
    private int mSeekPosition;

    private static final String SEEK_POSITION_KEY = "SEEK_POSITION_KEY";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        video_layout=findViewById(R.id.video_layout);
        llbottom=findViewById(R.id.llbottom);



        mVideoView = (UniversalVideoView) findViewById(R.id.videoView);
        mMediaController = (UniversalMediaController) findViewById(R.id.media_controller);
        mVideoView.setMediaController(mMediaController);

        setVideoAreaSize();
        mVideoView.start();



        mVideoView.setVideoViewCallback(new UniversalVideoView.VideoViewCallback() {




            @Override
            public void onScaleChange(boolean isFullscreen) {
                isFullscreen = isFullscreen;
                if (isFullscreen) {

                    ViewGroup.LayoutParams layoutParams = video_layout.getLayoutParams();
                    layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                    layoutParams.height =ViewGroup.LayoutParams.MATCH_PARENT;
                    video_layout.setLayoutParams(layoutParams);
                    //GONE the unconcerned views to leave room for video and controller
                    llbottom.setVisibility(View.GONE);
                } else {
                    ViewGroup.LayoutParams layoutParams = video_layout.getLayoutParams();
                    layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                    layoutParams.height = cachedHeight;
                    video_layout.setLayoutParams(layoutParams);
                    llbottom.setVisibility(View.VISIBLE);
                }

                switchTitleBar(!isFullscreen);
            }

            private void switchTitleBar(boolean show) {
                android.support.v7.app.ActionBar supportActionBar = getSupportActionBar();
                if (supportActionBar != null) {
                    if (show) {
                        supportActionBar.show();
                    } else {
                        supportActionBar.hide();
                    }
                }
            }
            @Override
            public void onPause(MediaPlayer mediaPlayer) { // Video pause
                Log.d(TAG, "onPause UniversalVideoView callback");
                mediaPlayer.pause();
            }

            @Override
            public void onStart(MediaPlayer mediaPlayer) { // Video start/resume to play
                Log.d(TAG, "onStart UniversalVideoView callback");
                mediaPlayer.start();
            }

            @Override
            public void onBufferingStart(MediaPlayer mediaPlayer) {// steam start loading
                Log.d(TAG, "onBufferingStart UniversalVideoView callback");
            }

            @Override
            public void onBufferingEnd(MediaPlayer mediaPlayer) {// steam end loading
                Log.d(TAG, "onBufferingEnd UniversalVideoView callback");
            }

        });
    }
    @Override
    public void onBackPressed() {
        if (this.isFullscreen) {
            mVideoView.setFullscreen(false);
        } else {
            super.onBackPressed();
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState Position=" + mVideoView.getCurrentPosition());
        outState.putInt(SEEK_POSITION_KEY, mSeekPosition);
    }

    @Override
    protected void onRestoreInstanceState(Bundle outState) {
        super.onRestoreInstanceState(outState);
        mSeekPosition = outState.getInt(SEEK_POSITION_KEY);
        Log.d(TAG, "onRestoreInstanceState Position=" + mSeekPosition);
    }
    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause ");
        if (mVideoView != null && mVideoView.isPlaying()) {
            mSeekPosition = mVideoView.getCurrentPosition();
            Log.d(TAG, "onPause mSeekPosition=" + mSeekPosition);
            mVideoView.pause();
        }
    }

    private void setVideoAreaSize() {
        video_layout.post(new Runnable() {
            @Override
            public void run() {
                int width = video_layout.getWidth();
                cachedHeight = (int) (width * 405f / 720f);
//                cachedHeight = (int) (width * 3f / 4f);
//                cachedHeight = (int) (width * 9f / 16f);
                ViewGroup.LayoutParams videoLayoutParams = video_layout.getLayoutParams();
                videoLayoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                videoLayoutParams.height = cachedHeight;
                video_layout.setLayoutParams(videoLayoutParams);
                mVideoView.setVideoPath((TEST_URL));
                mVideoView.requestFocus();
            }
        });
    }

}
